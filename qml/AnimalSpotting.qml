import QtQuick 2.0
import Sailfish.Silica 1.0
import "pages"

ApplicationWindow{
    id: animalSpottingWindow

    initialPage: Component { InitialPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations
}
