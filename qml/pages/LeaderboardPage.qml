import QtQuick 2.0
import Sailfish.Silica 1.0

Page{
    id: page
    SilicaListView{
        anchors.fill: page
        Column{
            spacing: Theme.paddingLarge
            width: parent.width
            PageHeader{
                title: qsTr("Huippupisteet")
            }
            /*
              --Kettu--
              */
            Row{
                spacing: Theme.paddingLarge
                Image {
                    id: profilePic1
                    height: 250
                    width: 250

                    source: "../../imgs/susi_mustaOutline.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //GalleryModel.currentIndex = index
                            pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: parent.verticalCenter

                    Label{
                        text: qsTr("Masa")
                        font.pixelSize: Theme.fontSizeLarge
                    }
                    Label{
                        text: qsTr("Pisteet: 20")
                    }
                }


            }

        }



        Rectangle {
            color: Theme.highlightDimmerColor
            anchors{

                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                //centerIn: parent
            }
            height: page.height / 4
            width: page.width

            Row{
                spacing: Theme.paddingLarge
                Image {
                    id: profilePic
                    height: 250
                    width: 250

                    source: "../../imgs/kettu_outline_musta.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //GalleryModel.currentIndex = index
                            pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: parent.verticalCenter
                    spacing: Theme.paddingLarge

                    Label{
                        text: qsTr("A Manninen")
                        color: Theme.highlightColor
                        font.pixelSize: Theme.fontSizeLarge
                    }
                    Label{
                        text: qsTr("Pisteet: 65")
                    }
                    Label{
                        text: qsTr("Sijoituksesi: 47")
                        font.pixelSize: Theme.fontSizeLarge
                    }
                }


            }

        }
    }
}
