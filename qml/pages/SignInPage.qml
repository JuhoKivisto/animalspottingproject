import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    id: textInputPage

    property var textAlignment: undefined

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge

        PullDownMenu {
            MenuItem {
                text: "Kirjaudu sisään"
                onClicked: pageStack.push(Qt.resolvedUrl("InitialPage.qml"))
            }

        }

        VerticalScrollDecorator {}

        Column {
            id: column
            width: parent.width

            PageHeader { title: "Sisään kirjautuminen" }



            TextField {
                focus: true
                label: "Käyttäjä tunnus"
                placeholderText: label
                width: parent.width
                horizontalAlignment: textAlignment
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: passwordField.focus = true
            }

            PasswordField {
                id: passwordField
                width: parent.width
                horizontalAlignment: textAlignment
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: numberField.focus = true
            }


        }
    }
}
