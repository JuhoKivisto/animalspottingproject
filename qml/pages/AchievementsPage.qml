import QtQuick 2.0
import Sailfish.Silica 1.0

Page{

    SilicaListView{
        anchors.fill: parent



        Column{
            spacing: Theme.paddingLarge
            width: parent.width
            PageHeader{
                title: qsTr("Saavutukset")
            }

            /*
              --Kettu--
              */
            Row{
                spacing: Theme.paddingLarge
                Image {
                    id: profilePic
                    height: 250
                    width: 250

                    source: "../../imgs/kettu_outline_musta.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //GalleryModel.currentIndex = index
                            pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: parent.verticalCenter

                    Label{
                        text: qsTr("Olet nähnyt ketun")
                    }
                    Label{
                        text: qsTr("")
                    }
                }


            }

            /*
              --Mäyrä--
              */
            Row{
                spacing: Theme.paddingLarge
                Image {
                    id: profilePic1
                    height: 250
                    width: 250

                    source: "../../imgs/Mayra_MustaOutline.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //GalleryModel.currentIndex = index
                            pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: parent.verticalCenter

                    Label{
                        text: qsTr("Olet nähnyt mäyrän")
                    }
                    Label{
                        text: qsTr("")
                    }
                }
            }

            /*
              --Susi--
              */
            Row{
                spacing: Theme.paddingLarge
                Image {
                    id: profilePic2
                    height: 250
                    width: 250

                    source: "../../imgs/susi_mustaOutline.png"

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            //GalleryModel.currentIndex = index
                            pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
                        }
                    }
                }

                Column{
                    anchors.verticalCenter: parent.verticalCenter

                    Label{
                        text: qsTr("Olet nähnyt suden")
                    }
                    Label{
                        text: qsTr("")
                    }
                }
            }
        }


    }

}
