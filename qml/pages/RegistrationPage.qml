import QtQuick 2.0
import Sailfish.Silica 1.0

Page {
    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height + Theme.paddingLarge

        PullDownMenu {
            MenuItem {
                text: "Luo tili"
                onClicked: pageStack.push(Qt.resolvedUrl("InitialPage.qml"))
            }

        }

        VerticalScrollDecorator {}

        Column {
            id: column
            anchors { left: parent.left; right: parent.right }
            spacing: Theme.paddingLarge

            PageHeader { title: "Tilin luonti" }

            TextField {
                id: firstname
                anchors { left: parent.left; right: parent.right }
                focus: true; label: "Etunimi"; placeholderText: label
                EnterKey.enabled: text || inputMethodComposing
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: lastname.focus = true
            }

            TextField {
                id: lastname
                anchors { left: parent.left; right: parent.right }
                label: "Sukunimi"; placeholderText: label
                EnterKey.enabled: text || inputMethodComposing
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: email.focus = true
            }

            TextField {
                id: email
                anchors { left: parent.left; right: parent.right }
                label: "Sähköpostiosoite"; placeholderText: label
                EnterKey.enabled: text || inputMethodComposing
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: password.focus = true
            }

            TextField {
                id: password
                anchors { left: parent.left; right: parent.right }
                echoMode: TextInput.Password
                label: "Salasana"; placeholderText: label
                EnterKey.enabled: text || inputMethodComposing
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: confirmPassword.focus = true
            }

            TextField {
                id: confirmPassword
                anchors { left: parent.left; right: parent.right }
                echoMode: TextInput.Password; enabled: password.text || text
                errorHighlight: password.text != text
                label: "Varmista salasana"
                placeholderText: label; opacity: enabled ? 1 : 0.5
                Behavior on opacity { FadeAnimation { } }
                EnterKey.enabled: text || inputMethodComposing
                EnterKey.highlighted: !errorHighlight
                EnterKey.iconSource: "image://theme/icon-m-enter-next"
                EnterKey.onClicked: {
                    if (errorHighlight)
                        firstname.focus = true
                    else
                        pageStack.push(nextPage, {}, PageStackAction.Animated)
                }
            }
        }
    }

    Component {
        id: nextPage
        Page {
            backNavigation: true

            Column {
                anchors { left: parent.left; right: parent.right }
                spacing: Theme.paddingLarge

                PageHeader { title: "Account Created" }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    horizontalAlignment: Text.AlignHCenter
                    text: "User Information"
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    horizontalAlignment: Text.AlignHCenter
                    text: firstname.text
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    horizontalAlignment: Text.AlignHCenter
                    text: lastname.text
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    horizontalAlignment: Text.AlignHCenter
                    text: email.text
                }
            }
        }
    }
}
