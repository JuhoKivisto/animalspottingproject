import QtQuick 2.0
import Sailfish.Silica 1.0

Page{
    id: page

    SilicaFlickable{
        width: parent.width
        height: parent.height
        anchors.fill: parent
        contentWidth: parent.width
        contentHeight: col.height + Theme.paddingLarge
        PullDownMenu {
            MenuItem {
                text: "Lähetä"
                onClicked: pageStack.push(Qt.resolvedUrl("InitialPage.qml"))
            }

        }

        Column{
            id: col
            spacing: Theme.paddingLarge
            width: parent.width
            Rectangle {
                color: Theme.highlightColor
                anchors.horizontalCenter: parent.horizontalCenter
                height: Theme.itemSizeSmall
                width: page.width
                Label {
                    text: "Pisteet: 7"
                    anchors.centerIn: parent

                    //color: "green"
                }
            }

            Image {
                id: name
                source: "../../imgs/kettu.PNG"
                width: page.width
                height: page.height / 1.5

            }
        }

        Column{
            width: parent.width
            height: parent.height
            anchors.bottom: parent.bottom

            //            ComboBox {
            //                width: page.width
            //                label: "Eläinlaji"
            //                menu: ContextMenu {
            //                    MenuItem { text: "Ei mikään"}
            //                    MenuItem { text: "Hirvi" }
            //                    MenuItem { text: "Susi" }
            //                    MenuItem { text: "Karhu" }
            //                    MenuItem { text: "Supikoira" }
            //                    MenuItem { text: "Orava" }
            //                    MenuItem { text: "Hiiri" }

            //                    //onClicked: animal1.activeFocus

            //                }
            //            }


//            TextSwitch{
//                id: animal1
//                text: qsTr("Kettu")

//            }
//            TextSwitch{
//                id: animal2
//                text: qsTr("Piisami-Sami")

//            }

            //            Row{

            //                Label{
            //                    TextSwitch{
            //                        id: animal3
            //                        text: qsTr("Rotta");

            //                    }
            //                    TextSwitch{
            //                        id: animal4
            //                        text: qsTr("Rotta");

            //                    }
            //                }


            //            }




        }
    }

}
