import QtQuick 2.0
import Sailfish.Silica 1.0

Page{
    Column{
        PageHeader{
            id: header
            title: qsTr("Asetukset")
        }

        id: mainColumn
        width: parent.width
        height:mainColumn.height + Theme.paddingLarge
        spacing: Theme.paddingLarge

        Slider {
            id: slider
            label: "Äänet"
            width: parent.width
            minimumValue: 0; maximumValue: 100; stepSize: 1
            valueText: value + " %"
        }
    }
}
