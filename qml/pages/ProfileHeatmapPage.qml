import QtQuick 2.0
import Sailfish.Silica 1.0

Page{
    id: page

    Column{

        PageHeader{
            id: header
            title: "Tilin lämpökartta"
        }
        Image {
            id: name
            source: "../../imgs/Kartta.PNG" // Kartta
            width: page.width
            height: page.height / 1.5

        }
    }

    Column{
        height: parent.height / 5
        width: parent.width

        anchors{
            bottom: parent.bottom
        }

        Image {
            id: profilePic
            height: 250
            width: 250

            source: "../../imgs/kettu_outline_musta.png"
        }
    }

    Column{
        height: parent.height / 5
        width: parent.width
        //spacing: Theme.paddingMedium

        anchors{
            bottom: parent.bottom

        }

        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("A Manninen")
            font.pixelSize: Theme.fontSizeLarge


        }
        DetailItem {
            x: Theme.paddingLarge + 50
            label: "Viimeksi paikalla:"
            value: "2 päivää sitten"

        }


    }



}
