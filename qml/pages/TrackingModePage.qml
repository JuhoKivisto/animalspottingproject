import QtQuick 2.0
import Sailfish.Silica 1.0

Page{
    id: page



    Column{

        PageHeader{
            id: header
            title: "Jäljitys tila"
        }
        Image {
            id: name
            source: "../../imgs/kartta_valmis.png"
            width: page.width
            height: page.height / 1.5

        }
    }

    Column{
        height: parent.height / 4.5
        width: parent.width
        anchors{


            bottom: parent.bottom
            left: parent.left
        }

        DetailItem {
            x: -150
            label: "Kuljettu matka:"
            value: "1,77 km"

        }
    }

    Column{
        id: cameraButton
        anchors{

            right:  parent.right
            bottom: parent.bottom
        }
        //anchors.bottom: parent.bottom

        height: parent.height / 10

        Button{
            //height: 200
            width: 200
            color: "white"


            id: takePicture

            //onClicked: camera.imageCapture


            Image {
                id: cameraButtonI
                height: parent.height
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter

                source: "../../imgs/fox.png"
            }
            onClicked: pageStack.push(Qt.resolvedUrl("InitialPage.qml"))
        }


    }

    Column{
        height: parent.height / 6
        width: parent.width

        anchors{
            bottom: parent.bottom
        }

        Image {
            id: profilePic
            height: 250
            width: 250

            source: "../../imgs/kettu_outline_musta.png"

            MouseArea {
                anchors.fill: parent
                onClicked: {
                    //GalleryModel.currentIndex = index
                    window.pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
                }
            }
        }
    }

    Column{
        height: parent.height / 7
        width: parent.width / 1.1
        //spacing: Theme.paddingMedium

        anchors{
            bottom: parent.bottom

        }

        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("A Manninen")
            font.pixelSize: Theme.fontSizeLarge


        }



    }


}
