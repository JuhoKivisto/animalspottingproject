import QtQuick 2.0
import Sailfish.Silica 1.0



Page{

    onStatusChanged: {


        switch(status) {
        case PageStatus.Activating:
            if (animalSpottingWindow.justStarted === true) {
                animalSpottingWindow.initializeApplication()

            }

            break
        case PageStatus.Active:

            pageStack.pushAttached(Qt.resolvedUrl("ProfileHeatmapPage.qml"))
            //pageStack.pushAttached(Qt.resolvedUrl("FirstPage.qml"))
            break
        }
    }

    SilicaListView{
        anchors.fill: parent

        PageHeader {
            id: header
            title: qsTr("A Manninen")
            Image {
                id: profilePic
                height: parent.height
                width: parent.width / 4

                source: "../../imgs/kettu_outline_musta.png" // Profiili kuva
                MouseArea {
                    anchors.fill: parent
                    onClicked: {
                        //GalleryModel.currentIndex = index
                        window.pageStack.push(Qt.resolvedUrl("AchievementsPage.qml"))
                    }
                }
            }
        }

        Column{
            id: mainColumn
            width: parent.width
            height: header.height +Theme.paddingLarge
            spacing: Theme.paddingLarge









            //            Label {
            //                x: Theme.horizontalPageMargin
            //                anchors.horizontalCenter: parent.horizontalCenter
            //                text: qsTr("Käyttäjä 1")
            //                color: Theme.highlightColor
            //                font.pixelSize: Theme.fontSizeExtraLarge
            //            }


            //            ListItem{
            //                id: listEntry0
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("3 x Kettu")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry1
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("1 x Karhu")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }


            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Poista"
            //                        onClicked: {
            //                            listEntry.remorseAction("Poistetaan", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry2
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("1 x Piisami")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry3
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("3 x Rotta")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry4
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("3 x Lokki")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry5
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("3 x Kissa")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry6
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("1 x Hirvi")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }
            //            ListItem{
            //                id: listEntry7
            //                width: parent.width

            //                Label {
            //                    color: listEntry.highlighted ? Theme.highlightColor :Theme.primaryColor
            //                    text: qsTr("1 x Kettu")
            //                    x: Theme.horizontalPageMargin
            //                    anchors.verticalCenter: parent.verticalCenter
            //                }



            //                menu: ContextMenu{
            //                    MenuItem{
            //                        text: "Remove"
            //                        onClicked: {
            //                            listEntry.remorseAction("Deleting", function(){
            //                                listModel.remove(model.index)
            //                            })
            //                        }
            //                    }
            //                }
            //            }






        }

        Column{
            height: parent.height / 10
            width: parent.width / 1.5
            //x: Theme.horizontalPageMargin
            anchors{


                bottom: parent.bottom
                right: parent.right
            }
            Label {

                text: qsTr("Yhteispisteet: 38")
                color: Theme.highlightColor
                font.pixelSize: Theme.fontSizeExtraLarge
            }
        }

        Column{
            height: parent.height / 1.2
            width: parent.width
            id: animal
            anchors{

                bottom: parent.bottom

            }
            spacing: Theme.paddingLarge

            Row{
                id: headRow
                spacing: Theme.paddingLarge + 100
                x: Theme.horizontalPageMargin
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("Määrä")
                    color: Theme.highlightColor
                }
                Label{

                    text: qsTr("Eläin")
                    color: Theme.highlightColor
                }
            }

            Row{
                id: row0
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }
                Label{

                    text: qsTr("Karhu")
                }
            }
            Row{
                id: row1
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }
                Label{

                    text: qsTr("Piisami")
                }
            }
            Row{
                id: row2
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("3")
                }
                Label{

                    text: qsTr("Rotta")
                }
            }
            Row{
                id: row3
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }
                Label{

                    text: qsTr("Lokki")
                }
            }
            Row{
                id: row4
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }
                Label{

                    text: qsTr("Kissa")
                }
            }
            Row{
                id: row5
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }
                Label{

                    text: qsTr("Kissa")
                }
            }
            Row{
                id: row6
                spacing: Theme.paddingLarge + 150
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }
                Label{

                    text: qsTr("Kissa")
                }
            }



        }

        Column{
            id: animalLeft
            height: parent.height / 1.2
            width: parent.width / 2.5

            anchors{

                bottom: parent.bottom
                right: parent.right

            }
            spacing: Theme.paddingLarge

            Row{
                id: headRowLeft
                spacing: Theme.paddingLarge
                x: Theme.horizontalPageMargin
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("Pisteet")
                    color: Theme.highlightColor
                }

            }

            Row{
                id: row0Left
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("15")
                }

            }

            Row{
                id: row0Left1
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("10")
                }

            }Row{
                id: row0Left2
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("7")
                }

            }Row{
                id: row0Left3
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("3")
                }

            }Row{
                id: row0Left4
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("2")
                }

            }Row{
                id: row0Left5
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }

            }Row{
                id: row0Left6
                spacing: Theme.paddingLarge + 50
                x: Theme.horizontalPageMargin + 50
                //anchors.horizontalCenter: parent.horizontalCenter
                Label{

                    text: qsTr("1")
                }

            }

        }


        VerticalScrollDecorator{}

    }



}
