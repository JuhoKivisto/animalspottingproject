import QtQuick 2.0
import QtMultimedia 5.0
import Sailfish.Silica 1.0

Page{
    id: page



    onStatusChanged: {


        switch(status) {
        case PageStatus.Activating:
            if (animalSpottingWindow.justStarted === true) {
                animalSpottingWindow.initializeApplication()

            }

            break
        case PageStatus.Active:



            if  (!pageContainer.contains(("TrackingModePage.qml"))){

                pageStack.nextPage(Qt.resolvedUrl("TrackingModePage.qml"))
            }

            else{
                //pageStack.
                pageStack.pushAttached(Qt.resolvedUrl("TrackingModePage.qml"))
            }
            break
        }
    }

    function switchPage(pageItem){
        if(PageStatus.Active){
            if(!pageContainer.contains((pageItem))){
                pageStack.nextPage(Qt.resolvedUrl(pageItem))
            }

            else{

                pageStack.push(pageItem)
            }


        }
    }

    SilicaListView{
        anchors.fill: page

        PullDownMenu {
            MenuItem {
                text: "Asetukset"
                onClicked: switchPage(Qt.resolvedUrl("SettingsPage.qml"))
            }
            MenuItem {
                text: "Tili"
                onClicked: pageStack.push(Qt.resolvedUrl("ProfilePage.qml"))
            }
            MenuItem {
                text: "Registeröi tili"
                onClicked: pageStack.push(Qt.resolvedUrl("RegistrationPage.qml"))

            }
            MenuItem {
                text: "Kirjaudu sisään"
                onClicked: pageStack.push(Qt.resolvedUrl("SignInPage.qml"))

            }
            MenuItem {
                text: "Huippupisteet"
                onClicked: switchPage(Qt.resolvedUrl("LeaderboardPage.qml"))

            }
        }

        //        ViewPlaceholder{
        //            enabled: true
        //            text: qsTr("Kamera")

        //        }

        header: Column {
            width: parent.width
            height: header.height + mainColumn.height + Theme.paddingLarge
            PageHeader {
                id: header
                title: "Kuvaus tila"
            }

            Column{
                id: mainColumn
                //height: parent.height
                spacing: Theme.paddingLarge
                width: parent.width





            }



            Camera {



                id: camera

                imageProcessing.whiteBalanceMode: CameraImageProcessing.WhiteBalanceFlash

                exposure {
                    exposureCompensation: -1.0
                    exposureMode: Camera.ExposurePortrait
                }

                flash.mode: Camera.FlashRedEyeReduction

                imageCapture {
                    onImageCaptured: {
                        photoPreview.source = preview  // Show the preview in an Image
                    }
                }
            }

            VideoOutput {
                source: camera
                //anchors.fill: parent
                focus : visible // to receive focus and capture key events when visible
            }

            Image {
                id: photoPreview

            }

        }
        Rectangle {
            color: Theme.highlightDimmerColor
            anchors{

                horizontalCenter: parent.horizontalCenter
                bottom: parent.bottom
                //centerIn: parent
            }
            height: page.height / 4
            width: page.width


            Column{
                id: cameraButton
                anchors{

                    right:  parent.right
                    bottom: parent.bottom
                }
                //anchors.bottom: parent.bottom

                height: parent.height / 4

                Button{
                    //height: 200
                    width: 200
                    color: "white"


                    id: takePicture

                    //onClicked: camera.imageCapture


                    Image {
                        id: profilePic
                        height: parent.height
                        width: parent.width
                        anchors.horizontalCenter: parent.horizontalCenter

                        source: "../../imgs/fox.png"
                    }
                    onClicked: pageStack.push(Qt.resolvedUrl("PhotoPage.qml"))
                }


            }




            Column{
                height: parent.height / 4.5
                width: parent.width / 2
                anchors{
                    //bottom: parent.bottom
                }




                TextSwitch{
                    id: animal1
                    text: qsTr("Kettu");

                }
                TextSwitch{
                    id: animal2
                    text: qsTr("Piisami");
                }
                TextSwitch{
                    id: animal3
                    text: qsTr("Rotta");

                }



            }

            Column{
                height: parent.height / 4.5
                width: parent.width / 2
                anchors{
                    //horizontalCenter: parent.horizontalCenter
                    //bottom: parent.bottom
                    right:  parent.right
                }

                ComboBox {
                    width: page.width
                    label: "Muu eläin:"
                    menu: ContextMenu {
                        MenuItem { text: "Ei mikään"}
                        MenuItem { text: "Hirvi" }
                        MenuItem { text: "Susi" }
                        MenuItem { text: "Karhu" }
                        MenuItem { text: "Supikoira" }
                        MenuItem { text: "Orava" }
                        MenuItem { text: "Hiiri" }

                        //onClicked: animal1.activeFocus

                    }
                }




            }
        }



    }
}
