# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = AnimalSpotting

CONFIG += sailfishapp

SOURCES += src/AnimalSpotting.cpp

DISTFILES += qml/AnimalSpotting.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    qml/pages/SecondPage.qml \
    rpm/AnimalSpotting.changes.in \
    rpm/AnimalSpotting.changes.run.in \
    rpm/AnimalSpotting.spec \
    rpm/AnimalSpotting.yaml \
    translations/*.ts \
    AnimalSpotting.desktop \
    qml/pages/ProfilePage.qml \
    qml/pages/SettingsPage.qml \
    qml/pages/InitialPage.qml \
    qml/pages/TrackingModePage.qml \
    imgs/kartta_valmis.png \
    imgs/fox.png \
    qml/pages/ProfileHeatmapPage.qml \
    imgs/Kartta.PNG \
    qml/pages/PhotoPage.qml \
    imgs/kettu.PNG \
    qml/pages/RegistrationPage.qml \
    qml/pages/SignInPage.qml \
    qml/pages/AchievementsPage.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/AnimalSpotting-de.ts
